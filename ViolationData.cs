﻿using System.Collections.Generic;

namespace Station_Side_Software
{
    public class ViolationData
    {       

        public string PlateNumber
        {            
            get;
            set;
        }

        public int Code
        {
            get;
            set;
        }

        public string Region
        {
            get;
            set;
        }

        public string Zone
        {
            get;
            set;
        }

        public string Time
        {
            get;
            set;
        }

        public string Date
        {
            get;
            set;
        }


        public double Latitude
        {
            get;
            set;
        }

        public double Longitude
        {
            get;
            set;
        }

        public string ViolationLevel
        {
            get;
            set;
        }
    }

    public class ViolationDataList : List<ViolationData> { }
}
