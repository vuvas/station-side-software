﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Station_Side_Software
{
    public class DatabaseClass
    {
        private string connectionString = @"Data Source=.\;Initial Catalog=TapellaSystemDB;Integrated Security=true";
        private SqlConnection sqlConnection;
        private SqlCommand sqlCommand;
        private SqlDataAdapter sqlDataAdapter;

        public void SetConnection(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void BindComboBox(System.Windows.Controls.ComboBox comboBoxName)
        {
            sqlConnection = new SqlConnection(connectionString);
            try
            {

                SqlDataAdapter da = new SqlDataAdapter("Select pathID FROM Path", sqlConnection);
                DataSet ds = new DataSet();
                da.Fill(ds, "paths");
                comboBoxName.ItemsSource = ds.Tables[0].DefaultView;
                comboBoxName.DisplayMemberPath = ds.Tables[0].Columns["pathID"].ToString();
                comboBoxName.SelectedValuePath = ds.Tables[0].Columns["pathID"].ToString();
            }
            catch { }
            finally
            {
                sqlConnection.Close();
            }
        }

        public int InsertNewRecord(string[] aMessage, string tableName)
        {
            int rows = 0;
            string sql = string.Empty;

            switch (tableName)
            {
                case "ViolationRecordTable":
                        sql = "INSERT INTO ViolationRecordTable VALUES('" +
                        aMessage[0] + "'," +
                        int.Parse(aMessage[1]) + ",'" +
                        aMessage[2] + "','" +
                        aMessage[3] + "','" +
                        aMessage[4] + "','" +
                        aMessage[5] + "'," +
                        float.Parse(aMessage[6]) + "," +
                        float.Parse(aMessage[7]) + ",'" +
                        aMessage[8] + "')";

                        try
                        {
                            sqlConnection = new SqlConnection(connectionString);
                            sqlCommand = sqlConnection.CreateCommand();
                            sqlCommand.CommandText = sql;
                            sqlConnection.Open();

                            rows = sqlCommand.ExecuteNonQuery();

                        }
                        catch (Exception)
                        {
                            System.Windows.MessageBox.Show("Error in database connection.", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                        }
                        finally
                        {
                            sqlConnection.Close();
                        }

                    break;

                case "Path":
                    sql = "INSERT INTO Path VALUES(" +
                        int.Parse(aMessage[0]) + ",'" +
                        aMessage[1] + "','" +
                        aMessage[2] + "')";
                    
                    try
                    {
                        sqlConnection = new SqlConnection(connectionString);
                        sqlCommand = sqlConnection.CreateCommand();
                        sqlCommand.CommandText = sql;
                        sqlConnection.Open();

                        rows = sqlCommand.ExecuteNonQuery();

                    }
                    catch (Exception)
                    {
                        System.Windows.MessageBox.Show("Error in database connection.", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                    break;

                case "Vehicle":
                    sql = "INSERT INTO Vehicle VALUES(" +
                        int.Parse(aMessage[0]) + ","+
                        int.Parse(aMessage[1]) + ",'" +
                        aMessage[2] + "','" +
                        aMessage[3] + "'," +
                        int.Parse(aMessage[4]) + ")";
                    System.Windows.MessageBox.Show(sql);
                    try
                    {
                        sqlConnection = new SqlConnection(connectionString);
                        sqlCommand = sqlConnection.CreateCommand();
                        sqlCommand.CommandText = sql;
                        sqlConnection.Open();

                        rows = sqlCommand.ExecuteNonQuery();

                    }
                    catch (Exception)
                    {
                        System.Windows.MessageBox.Show("Error in database connection.", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                    break;
                default:
                    System.Windows.MessageBox.Show("Invalid Table Name", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                    break;                    
            }

            return rows;
        }

        public DataTable BindViolationTable()
        {
            string sql = "SELECT plateNo as [Plate No],code as [Code],region as [Region],zone as [Zone],violationTime as [Violation Time],violationDate as [Violation Date], latitude as [Latitude], longitude as [Longitude], violationLevel as [Violation Level] "
                + "FROM ViolationRecordTable";
            DataTable dataTable = new DataTable();
            DataSet dataSet = new DataSet();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlCommand = sqlConnection.CreateCommand();
                sqlCommand.CommandText = sql;
                sqlConnection.Open();

                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataSet,"ViolationRecords");
                dataTable = dataSet.Tables["ViolationRecords"];
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString());
            }
            finally
            {
                sqlConnection.Close();
            }

            return dataTable;
        }

        public DataTable BindTodaysViolation()
        {
            string sql = "SELECT plateNo as [Plate No],code as [Code],region as [Region],zone as [Zone],violationTime as [Violation Time],violationDate as [Violation Date], latitude as [Latitude], longitude as [Longitude], violationLevel as [Violation Level] "
                + "FROM ViolationRecordTable WHERE violationDate = '" + DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year + "'";
            DataTable dataTable = new DataTable();
            DataSet dataSet = new DataSet();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlCommand = sqlConnection.CreateCommand();
                sqlCommand.CommandText = sql;
                sqlConnection.Open();

                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataSet, "TodaysViolationRecords");
                dataTable = dataSet.Tables["TodaysViolationRecords"];
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString());
            }
            finally
            {
                sqlConnection.Close();
            }

            return dataTable;
        }

        public DataTable BindVehicleTable()
        {
            string sql = "SELECT plateNo as [Plate No],code as [Code],region as [Region],zone as [Zone],pathID as [Path ID] "
                       + "FROM Vehicle";
            DataTable dataTable = new DataTable();
            DataSet dataSet = new DataSet();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlCommand = sqlConnection.CreateCommand();
                sqlCommand.CommandText = sql;
                sqlConnection.Open();

                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataSet, "Vehicles");
                dataTable = dataSet.Tables["Vehicles"];
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString());
            }
            finally
            {
                sqlConnection.Close();
            }

            return dataTable;
        }

        public DataTable BindPathTable()
        {
            string sql = "SELECT pathID as [Path ID],zone as [Zone],checkPoints as [Checkpoints] "
                       + "FROM Path";
            DataTable dataTable = new DataTable();
            DataSet dataSet = new DataSet();

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlCommand = sqlConnection.CreateCommand();
                sqlCommand.CommandText = sql;
                sqlConnection.Open();

                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataSet, "Paths");
                dataTable = dataSet.Tables["Paths"];
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString());
            }
            finally
            {
                sqlConnection.Close();
            }

            return dataTable;
        }

        public DataTable SearchBy(string searchBy, string item)
        {
            string sql = "SELECT plateNo as [Plate No],code as [Code],region as [Region],zone as [Zone],violationTime as [Violation Time],violationDate as [Violation Date], latitude as [Latitude], longitude as [Longitude], violationLevel as [Violation Level] "
                + "FROM ViolationRecordTable";
            bool errorSearchKey = false;

            switch (searchBy)
            {
                case "Plate Number":
                    sql += " WHERE plateNo = '" + item + "'";
                    break;

                case "Region":
                    sql += " WHERE region = '" + item + "'";
                    break;

                case "Zone":
                    sql += " WHERE zone = '" + item + "'";
                    break;

                case "Date":
                    sql += " WHERE violationDate = '" + item + "'";
                    break;

                case "Violation Level":
                    sql += " WHERE violationLevel = '" + item + "'";
                    break;

                default:
                    System.Windows.MessageBox.Show("Invalid Search Operation", "Error",System.Windows.MessageBoxButton.OK,System.Windows.MessageBoxImage.Error);
                    errorSearchKey = true;
                    break;
            }

            DataTable dataTable = new DataTable();
            DataSet dataSet = new DataSet();
            
            if (!errorSearchKey)
            {
                try
                {
                    sqlConnection = new SqlConnection(connectionString);
                    sqlCommand = sqlConnection.CreateCommand();
                    sqlCommand.CommandText = sql;
                    sqlConnection.Open();

                    sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(dataSet, "ViolationRecords");
                    dataTable = dataSet.Tables["ViolationRecords"];
                }

                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }

                finally
                {
                    sqlConnection.Close();
                }
            }

            return dataTable;
        }

        public DataTable GetVehicle(string plateNo, string code)
        {
            DataTable dataTable = new DataTable();
            DataSet dataSet = new DataSet();
            string sql = "SELECT* FROM Vehicle WHERE plateNo='" + plateNo + "' AND code='" + code+"'";

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlCommand = sqlConnection.CreateCommand();
                sqlCommand.CommandText = sql;
                sqlConnection.Open();

                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataSet, "Vehicles");
                dataTable = dataSet.Tables["Vehicles"];
            }

            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }

            finally
            {
                sqlConnection.Close();
            }
            return dataTable;
        }

        public DataTable GetPath(int pathID)
        {
            DataTable dataTable = new DataTable();
            DataSet dataSet = new DataSet();
            string sql = "SELECT* FROM Path WHERE pathID="+ pathID;

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlCommand = sqlConnection.CreateCommand();
                sqlCommand.CommandText = sql;
                sqlConnection.Open();

                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dataSet, "Paths");
                dataTable = dataSet.Tables["Paths"];
            }

            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }

            finally
            {
                sqlConnection.Close();
            }
            return dataTable;
        }
    }
}
