﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Microsoft.Maps.MapControl.WPF;

namespace Station_Side_Software
{
    public class MapClassHelper
    {
        public void PlotPathInfo(LocationCollection pathCollection, Map myMap)
        {
            List<Pushpin> pinList = new List<Pushpin>();

            myMap.Children.Clear();  
            //myMap.Center = pathCollection[(pathCollection.Count) / 2];
            //myMap.ZoomLevel = 17.0;

            for (int i = 0; i < pathCollection.Count; i++)
            {
                Pushpin pins = new Pushpin();
                pins.Location = new Microsoft.Maps.MapControl.WPF.Location(pathCollection[i]);
                pinList.Add(pins);
                myMap.Children.Add(pinList[i]);
            }
        }

        public void PlotViolation(Microsoft.Maps.MapControl.WPF.LocationCollection violationLocationCollection, Map myMap)
        {
            if (violationLocationCollection.Count > 0)
            {
                myMap.Center = violationLocationCollection[(violationLocationCollection.Count) / 2];
                myMap.ZoomLevel = 17.0;
                myMap.Children.Clear();       //clear children from the map
                for (int i = 0; i < violationLocationCollection.Count; i++)
                {
                    PlotSingleViolation(violationLocationCollection[i], myMap);
                }
            }
        }

        public void PlotSingleViolation(Microsoft.Maps.MapControl.WPF.Location location, Map myMap)
        {
            Ellipse dot = new Ellipse();
            dot.Fill = new SolidColorBrush(Colors.Red);
            double radius = 6.0;
            dot.Width = radius * 2;
            dot.Height = radius * 2;
            ToolTip tt = new ToolTip();
            tt.Content = "Location = " + location;
            dot.ToolTip = tt;
            System.Windows.Point p0 = myMap.LocationToViewportPoint(location);
            System.Windows.Point p1 = new System.Windows.Point(p0.X - radius, p0.Y - radius);
            location = myMap.ViewportPointToLocation(p1);
            MapLayer.SetPosition(dot, location);
            myMap.Children.Add(dot);
        }



        public void PutPushpin(Microsoft.Maps.MapControl.WPF.Location location, Map myMap)
        {
            Pushpin pin = new Pushpin();
            pin.Location = location;
            myMap.Children.Add(pin);
        }
    }
}
