﻿using System.Text;
using System.Security.Cryptography;


namespace Station_Side_Software
{
    public class EncryptionHelper
    {

        Aes aes = Aes.Create();
        ICryptoTransform encrypter;
        ICryptoTransform decrypter;
        byte[] key = new byte[]{
                0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
                0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
                0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f};
        byte[] iv = new byte[]{
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

        public EncryptionHelper()
        {
            encrypter = aes.CreateEncryptor();
            decrypter = aes.CreateDecryptor();
            aes.Key = key;
            aes.IV  = iv;
            aes.Mode = CipherMode.ECB; ;
            aes.Padding = PaddingMode.None;
        }
        public byte[] EncryptString(string str)
        {
            byte[] buffer = Encoding.Default.GetBytes(str);
            buffer = encrypter.TransformFinalBlock(buffer, 0, buffer.Length);
            return buffer;

        }
        public byte[] DycryptString(string str)
        {
            byte[] buffer = Encoding.Default.GetBytes(str);
            buffer = decrypter.TransformFinalBlock(buffer, 0, buffer.Length);
            return buffer;
        }

           public byte[] EncryptByte(byte[] buffer)
        {
            return(encrypter.TransformFinalBlock(buffer, 0, buffer.Length));       

        }

        public byte[] DycryptByte(byte[] buffer)
        {
     
            return(decrypter.TransformFinalBlock(buffer, 0, buffer.Length));
            
    }
}
}