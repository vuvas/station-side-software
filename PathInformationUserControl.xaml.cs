﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace Station_Side_Software
{
    /// <summary>
    /// Interaction logic for PathInformationUserControl.xaml
    /// </summary>
    public partial class PathInformationUserControl : UserControl
    {
        public PathInformationUserControl()
        {
            InitializeComponent();
        }

        public void AddNewCheckPointContainer(CheckPointContainer checkPointContainer)
        {
            this.locationContainerPanel.Children.Add(checkPointContainer);
        }

        public int ChildrenCount()
        {
            return(this.locationContainerPanel.Children.Count);
        }


        public void Remove()
        {
            this.locationContainerPanel.Children.Clear();
        }
        //private void btnAddCheckPoint_Click(object sender, RoutedEventArgs e)
        //{
        //    AddNewCheckPointContainer(new CheckPointContainer());
        //}

        public List<CheckPointContainer> getCheckPoint()
        {
            List<CheckPointContainer> checkPoints = new List<CheckPointContainer>();

            foreach(var child in locationContainerPanel.Children)
            {
                CheckPointContainer ch = (CheckPointContainer)child;
                checkPoints.Add(ch);
            }            
            return checkPoints;
        }
    }
}
