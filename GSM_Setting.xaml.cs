﻿using System;
using System.Windows;
using ATSMS.Common;

namespace Station_Side_Software
{
    /// <summary>
    /// Interaction logic for GSM_Setting.xaml
    /// </summary>
    public partial class GSM_Setting : Window
    {
        private Main_Window mainWindow;

        public GSM_Setting()
        {
            InitializeComponent();
            LoadComboBoxes();
        }

        public GSM_Setting(Main_Window mainWindow)
        {
            this.mainWindow = mainWindow;
            InitializeComponent();
            LoadComboBoxes();
        }

        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            if (this.comboCOMPort.Text == string.Empty)
            {
                labelStatus.Content = "COM Port must be selected";             
                return;
            }

            mainWindow.oGsmModem.Port = this.comboCOMPort.Text;                      


            if (this.comboBaudRate.Text != string.Empty)
            {
                mainWindow.oGsmModem.BaudRate = Convert.ToInt32(this.comboBaudRate.Text);
            }

            if (this.comboDataBit.Text != string.Empty)
            {
                mainWindow.oGsmModem.DataBits = Convert.ToInt32(this.comboDataBit.Text);
            }

            if (this.comboStopBit.Text != string.Empty)
            {
                switch (this.comboStopBit.Text)
                {
                    case "1":
                        mainWindow.oGsmModem.StopBits = EnumStopBits.One;
                        break;

                    case "1.5":
                        mainWindow.oGsmModem.StopBits = EnumStopBits.OnePointFive;
                        break;

                    case "2":
                        mainWindow.oGsmModem.StopBits = EnumStopBits.Two;
                        break;
                }
            }

            if (this.comboFlowControl.Text != string.Empty)
            {
                switch (this.comboFlowControl.Text)
                {
                    case "None":
                        mainWindow.oGsmModem.FlowControl = EnumFlowControl.None;
                        break;

                    case "Hardware":
                        mainWindow.oGsmModem.FlowControl = EnumFlowControl.RTS_CTS;
                        break;

                    case "Xon/Xoff":
                        mainWindow.oGsmModem.FlowControl = EnumFlowControl.Xon_Xoff;
                        break;
                }
            }

            try
            {
                mainWindow.oGsmModem.Connect();

                Properties.Settings.Default.COMPort = mainWindow.oGsmModem.Port;
                Properties.Settings.Default.BaudRate = mainWindow.oGsmModem.BaudRate;
                Properties.Settings.Default.DataBit = mainWindow.oGsmModem.DataBits;
                Properties.Settings.Default.StopBit = Convert.ToInt32(mainWindow.oGsmModem.StopBits);
                Properties.Settings.Default.FlowControl = mainWindow.oGsmModem.FlowControl.ToString();
                Properties.Settings.Default.GSMConnected = true;

                //save setting
                Properties.Settings.Default.Save();
            }
            catch (Exception exception1)
            {
                // MessageBox.Show(exception1.Message);
                labelStatus.Content = exception1.Message;
                return;
            }

            try
            {
                mainWindow.oGsmModem.NewMessageIndication = true;
            }
            catch (Exception)
            { }

            this.btnCheckPhone.IsEnabled = true;
            this.btnDisconnect.IsEnabled = true;

            //MessageBox.Show("Connected to phone successfully !");
        }

        private void btnDisconnect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                mainWindow.oGsmModem.Disconnect();

                Properties.Settings.Default.GSMConnected = false;

                //save setting
                Properties.Settings.Default.Save();
            }
            catch (Exception exception1)
            {
                labelStatus.Content = exception1.Message;
            }
            this.btnCheckPhone.IsEnabled = false;
            this.btnDisconnect.IsEnabled = false;
            this.btnConnect.IsEnabled = true;
        }

        private void btnCheckPhone_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Going to analyze your phone. It may take a while");
            labelStatus.Content = "Going to analyze your phone. It may take a while ... ";
            mainWindow.oGsmModem.CheckATCommands();
            if (mainWindow.oGsmModem.ATCommandHandler.Is_SMS_Received_Supported)
            {
                //MessageBox.Show("Your phone is able to receive SMS. Message indication command is " + this.oGsmModem.ATCommandHandler.MsgIndication);
                labelStatus.Content = "Your phone is able to receive SMS. Message indication command is " + mainWindow.oGsmModem.ATCommandHandler.MsgIndication;
                mainWindow.oGsmModem.NewMessageIndication = true;
            }
            else
            {
                //MessageBox.Show("Sorry. Your phone cannot receive SMS");
                labelStatus.Content = "Sorry. Your phone cannot receive SMS";
            }
        }

        private void LoadComboBoxes()
        {
           this.comboCOMPort.Text = Properties.Settings.Default.COMPort;
           this.comboBaudRate.Text = Properties.Settings.Default.BaudRate.ToString();
           this.comboDataBit.Text = Properties.Settings.Default.DataBit.ToString();
           this.comboStopBit.Text = Properties.Settings.Default.StopBit.ToString();
           this.comboFlowControl.Text = Properties.Settings.Default.FlowControl.ToString();

           if (Properties.Settings.Default.GSMConnected)
           {
               this.btnCheckPhone.IsEnabled = true;
               this.btnDisconnect.IsEnabled = true;
               this.btnConnect.IsEnabled = false;
           }
           else
           {
               this.btnCheckPhone.IsEnabled = false;
               this.btnDisconnect.IsEnabled = false;
               this.btnConnect.IsEnabled = true;
           }
        }
    }
}
