﻿using System;
using System.Data;
using System.Windows;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Windows.Threading;
using System.Threading;

using ATSMS;
using Microsoft.Maps.MapControl.WPF;

namespace Station_Side_Software
{
    /// <summary>
    /// Interaction logic for Main_Window.xaml
    /// </summary>
    public partial class Main_Window : System.Windows.Window
    { 
        MapClassHelper mapClassHelper = new MapClassHelper();
        DatabaseClass databaseClass = new DatabaseClass();
        SD_Card sdCard = new SD_Card();

        //GSM
        public GSMModem oGsmModem = new GSMModem();
        GSM_Setting gsmSettingWindow;        
        
        //Notification
        private const double topOffset = 40;
        private const double leftOffset = 295;
        readonly GrowlNotifiactions growlNotifications = new GrowlNotifiactions();

        //other fields
        LocationCollection loggedViolationList;
        string recievedData = string.Empty;

        public Main_Window()
        {
            InitializeComponent();
            gsmSettingWindow = new GSM_Setting(this);
            this.oGsmModem.NewMessageReceived += new GSMModem.NewMessageReceivedEventHandler(this.oGsmModem_NewMessageReceived);

            //notification window placement
            growlNotifications.Top = SystemParameters.WorkArea.Top + topOffset;
            growlNotifications.Left = SystemParameters.WorkArea.Left + SystemParameters.WorkArea.Width - leftOffset;

            myMap.Center = new Microsoft.Maps.MapControl.WPF.Location(9.0408, 38.7628);
            myMap.ZoomLevel = 15.0;

            databaseClass.BindComboBox(comboPathID);
            databaseClass.BindComboBox(comboPathID2);
        }
        
        private void btnAddCheckPoint_Click(object sender, RoutedEventArgs e)
        {
            this.checkPointContainer.AddNewCheckPointContainer(new CheckPointContainer());
        }

        private void btnPlotPath_Click(object sender, RoutedEventArgs e)
        {
            List<CheckPointContainer> checkPoints = checkPointContainer.getCheckPoint();
            LocationCollection locList = new LocationCollection();

            foreach (CheckPointContainer ch in checkPoints)
            {
                if ((ch.GetLongitudeStr() != "") && (ch.GetLongitudeStr() != ""))
                {
                    Microsoft.Maps.MapControl.WPF.Location loc = new Microsoft.Maps.MapControl.WPF.Location(ch.GetLatitude(), ch.GetLongitude());
                    locList.Add(loc);
                }
            }
            mapClassHelper.PlotPathInfo(locList,myMap);
        }

        private void btnClearPath_Click(object sender, RoutedEventArgs e)
        {
            this.checkPointContainer.Remove(); //clear checkpoints from the container
            this.myMap.Children.Clear();       //clear pushpins from the map
        }

        private void btnSavePath_Click(object sender, RoutedEventArgs e)
        {
            if (txtPathID.Text != null && comboZone2.SelectedItem != null)
            {
                string [] aMessage = new string[3];
                aMessage[0] = txtPathID.Text;
                aMessage[1] = comboZone2.Text ;
                aMessage[2] = string.Empty;
                
                List<CheckPointContainer> checkPoints = checkPointContainer.getCheckPoint();
                foreach (CheckPointContainer ch in checkPoints)
                {
                    if ((ch.GetLongitudeStr() != "") && (ch.GetLongitudeStr() != ""))
                    {
                        aMessage[2] += ch.GetLatitudeStr() + "," + ch.GetLongitudeStr() + ",";
                    }
                }

                if (databaseClass.InsertNewRecord(aMessage, "Path") != 0)
                    statusTextBlock.Text="New Path is Saved into Database Successfully";
            }
            else
            {
                MessageBox.Show("Please Enter Path or Select Zone.", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
        }

        private void myMap_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (checkBoxUseMap.IsChecked == true)
            {
                e.Handled = true;
                int leftmarginPixel = (int)tabControlLeft.ActualWidth;
                System.Windows.Point p0 = e.GetPosition(this);

                System.Windows.Point p1 = new System.Windows.Point(p0.X - leftmarginPixel, p0.Y - 23);
                Microsoft.Maps.MapControl.WPF.Location loc1 = myMap.ViewportPointToLocation(p1);

                Pushpin piin = new Pushpin();
                piin.Location = loc1;
                myMap.Children.Add(piin);

                CheckPointContainer container = new CheckPointContainer();
                container.SetLatitude(loc1.Latitude);
                container.SetLongitude(loc1.Longitude);
                this.checkPointContainer.AddNewCheckPointContainer(container);
            }
        }

        private void btnSaveVehicle_Click(object sender, RoutedEventArgs e)
        {
            if (txtPlateNo.Text != null && comboCode.SelectedItem != null && comboRegion.SelectedItem != null && comboZone.SelectedItem != null && comboPathID.SelectedItem != null)
            {
                string[] aMessage = new string[5];
                aMessage[0] = txtPlateNo.Text;
                aMessage[1] = comboCode.Text;
                aMessage[2] = comboRegion.Text;
                aMessage[3] = comboZone.Text;
                aMessage[4] = comboPathID.Text;

                //insert into database
                if (databaseClass.InsertNewRecord(aMessage, "Vehicle") != 0)
                    statusTextBlock.Text = "New Vehicle is Saved into Database Successfully";
            }
            else
            {
                MessageBox.Show("Please Enter All the Necessary Fields Before Hitting the \"Save Vehicle\" button", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
            }
        }

        private void btnSaveToSDCard_Click(object sender, RoutedEventArgs e)
        {
            //Save to SD Card
            string otherInfo = string.Empty;
            string pathInfo = string.Empty;

            if (txtPlateNo.Text != null && comboCode2.Text != null && comboPathID2.Text != null)
            {
                System.Data.DataTable dataTable = databaseClass.GetVehicle(txtPlateNo2.Text, comboCode2.Text);
                otherInfo += dataTable.Rows[0]["plateNo"].ToString() + ",";
                otherInfo += dataTable.Rows[0]["code"].ToString() + ",";
                otherInfo += dataTable.Rows[0]["region"].ToString() + ",";
                otherInfo += dataTable.Rows[0]["zone"].ToString() + ",";
                otherInfo += dataTable.Rows[0]["pathID"].ToString();

                System.Data.DataTable dataTable2 = databaseClass.GetPath(int.Parse(comboPathID2.Text));
                string[] checkpoints = dataTable2.Rows[0]["checkPoints"].ToString().Split(',');

                for (int i = 0; i < checkpoints.Length; i++)
                {
                    if (i % 2 == 0)
                        pathInfo += checkpoints[i] + ",";
                    else
                        pathInfo += checkpoints[i] + "\r\n";
                }

                sdCard.WriteToSDCard(@"\pathInfo.txt", pathInfo);
                sdCard.WriteToSDCard(@"\otherInfo.txt", otherInfo);
                statusTextBlock.Text = "information is saved to SD Card successfully!";
            }
            
        }

        private void btnViewAll_Click(object sender, RoutedEventArgs e)
        {
            System.Data.DataTable allViolationDataTable = databaseClass.BindViolationTable();
            //this.dataGrid1.ItemsSource = allViolationDataTable.DataSet.Tables[0].DefaultView;
            this.dataGrid1.ItemsSource = allViolationDataTable.DefaultView;
            statusTextBlock.Text = "All violation records in the database";
        }

        private void btnViewToday_Click(object sender, RoutedEventArgs e)
        {
            System.Data.DataTable todaysViolationDataTable = databaseClass.BindTodaysViolation();
            this.dataGrid1.ItemsSource = todaysViolationDataTable.DefaultView;
            statusTextBlock.Text = "Today's violation records in the database";
        }

        private void btnViewAllPaths_Click(object sender, RoutedEventArgs e)
        {
            System.Data.DataTable allPathDataTable = databaseClass.BindPathTable();
            this.dataGrid1.ItemsSource = allPathDataTable.DefaultView;
            statusTextBlock.Text = "All Paths in the database";
        }

        private void btnViewAllVehicles_Click(object sender, RoutedEventArgs e)
        {
            System.Data.DataTable allVehicleDataTable = databaseClass.BindVehicleTable();
            this.dataGrid1.ItemsSource = allVehicleDataTable.DefaultView;
            statusTextBlock.Text = "All Vehicles in the database";
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (comboSearchBy.Text != null && txtItemToBeSearched.Text != null)
            {
                System.Data.DataTable searchResultTable = databaseClass.SearchBy(comboSearchBy.Text, txtItemToBeSearched.Text);
                this.dataGrid1.ItemsSource = searchResultTable.DefaultView;
                statusTextBlock.Text = searchResultTable.Rows.Count + " records found!!";
            }
        }

        private void btnReadViolation_Click(object sender, RoutedEventArgs e)
        {
            loggedViolationList = sdCard.ReadLogInfo();
            statusTextBlock.Text = loggedViolationList.Count.ToString() + " violation records in the SD Card.";
        }

        private void btnPlotViolation_Click(object sender, RoutedEventArgs e)
        {
            if (loggedViolationList.Count == 0)
            {
                MessageBox.Show("First read the violation records.");
                return;
            }
            else
            {
                mapClassHelper.PlotViolation(loggedViolationList,myMap);
            }
        }

        private void gsmSettingMenu_Click(object sender, RoutedEventArgs e)
        {
            gsmSettingWindow = new GSM_Setting(this);
            gsmSettingWindow.ShowDialog();
        }

        private void oGsmModem_NewMessageReceived(NewMessageReceivedEventArgs e)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Input, new ThreadStart(() =>
            {
                try
                {                    
                    recievedData = e.TextMessage;

                    if (recievedData != null)
                    {
                        string[] aMessage = recievedData.Split(',');
                        Console.WriteLine(recievedData);

                        //store into databse
                        databaseClass.InsertNewRecord(aMessage, "ViolationRecordTable");
                        growlNotifications.AddNotification(new Notification
                        {
                            Title = "Message:",
                            ImageUrl = "pack://application:,,,/Resources/notification-icon.png",
                            Message = "Plate No = " + aMessage[0] + "\n" + "Lat = " + aMessage[5] + "\n" + "Lon = " + aMessage[6]
                        });
                    }
                }
                catch
                {

                }
            }));
        }


        private void Window_Closing(object sender, CancelEventArgs e)
        {
            this.gsmSettingWindow.Close();
            this.growlNotifications.Close();
        }

        private void exportToExcelMenu_Click(object sender, RoutedEventArgs e)
        {
            ExportToExcel<ViolationData, ViolationDataList> s = new ExportToExcel<ViolationData, ViolationDataList>();
            DataView view = (DataView)dataGrid1.ItemsSource;
            System.Data.DataTable table = view.Table;
            DataRow[] rows = table.Select();

            ViolationDataList violationDataList = new ViolationDataList();

            foreach (DataRow row in rows)
            {
                ViolationData vioData = new ViolationData();

                object[] rowObjects = row.ItemArray;

                vioData.PlateNumber = rowObjects[0].ToString();
                vioData.Code = Convert.ToInt32(rowObjects[1]);
                vioData.Region = rowObjects[2].ToString();
                vioData.Zone = rowObjects[3].ToString();
                vioData.Time = rowObjects[4].ToString();
                vioData.Date = rowObjects[5].ToString();
                vioData.Latitude = Convert.ToDouble(rowObjects[6]);
                vioData.Longitude = Convert.ToDouble(rowObjects[7]);
                vioData.ViolationLevel = rowObjects[8].ToString();

                violationDataList.Add(vioData);

            }

            s.dataToPrint = violationDataList;
            s.GenerateReport();
        }

        private void printMenu_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
            if ((bool)printDialog.ShowDialog().GetValueOrDefault())
            {
                //Size pageSize = new Size(printDialog., printDialog.PrintableAreaHeight);

                ////sizing the element
                //dataGrid1.Measure(pageSize);
                //dataGrid1.Arrange(new Rect(5, 5, pageSize.Width, pageSize.Height));
                printDialog.PrintVisual(dataGrid1, "Datagrid Printing");
            }
        }

        private void dataGrid1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            
        }
    }
}
