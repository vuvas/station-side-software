﻿using System;
using System.Text;
using System.IO;
using Microsoft.Maps.MapControl.WPF;

namespace Station_Side_Software
{
    public class SD_Card
    {
        EncryptionHelper encryptionHelper = new EncryptionHelper();

        public string GetSDCardUrl()
        {
            string[] drives = Environment.GetLogicalDrives();
            string pathUrl = string.Empty;
            

            foreach (string drive in drives)
            {
                try
                {
                    DriveInfo di = new DriveInfo(drive);
                    if (di.VolumeLabel == "SD CARD")
                    {
                        pathUrl = di.RootDirectory.ToString();
                    }
                }
                catch (Exception)
                {                               
                }
            }
            return pathUrl;
        }

        public LocationCollection ReadLogInfo()
        {
            int latitudeIndex = 2;
            LocationCollection logInfoList = new LocationCollection();
            string sdCardUrl = GetSDCardUrl();
            sdCardUrl += "\\LOGINFO.txt";

            try
            {
                using (StreamReader sr = new StreamReader(sdCardUrl))
                {
                    string wholeDoc = sr.ReadToEnd();
                    string[] gpsLines = wholeDoc.Split('\n');

                    for (int i = 0; i < gpsLines.Length - 1; i++)
                    {
                        string[] stringLine = gpsLines[i].Split(',');
                        Microsoft.Maps.MapControl.WPF.Location location = new Microsoft.Maps.MapControl.WPF.Location(double.Parse(stringLine[latitudeIndex]), double.Parse(stringLine[latitudeIndex + 1]));
                        logInfoList.Add(location);

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

            return logInfoList;
        }

        public void WriteToSDCardEncrypted(string fileName, string text)
        {
            string sdCardUrl = GetSDCardUrl();
            if (sdCardUrl != null)
            {
                using (StreamWriter outfile = new StreamWriter(sdCardUrl + fileName))
                {
                    byte[] writable = encryptionHelper.EncryptString(text);
                    outfile.Write(Encoding.Default.GetString(writable));
                }
            }
        }

        public void WriteToSDCard(string fileName, string text)
        {
            string sdCardUrl = GetSDCardUrl();
            if (sdCardUrl != null)
            {
                using (StreamWriter outfile = new StreamWriter(sdCardUrl + fileName))
                {
                    outfile.Write(text);
                }
            }
        }
    }
}
