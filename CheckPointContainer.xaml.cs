﻿using System.Windows.Controls;

namespace Station_Side_Software
{
    /// <summary>
    /// Interaction logic for CheckPointContainer.xaml
    /// </summary>
    public partial class CheckPointContainer : UserControl
    {
        public CheckPointContainer()
        {
            InitializeComponent();
        }

        public void SetLatitude(double lat)
        {
            this.txtLatitude.Text = lat.ToString();
        }

        public void SetLongitude(double lon)
        {
            this.txtLongitude.Text = lon.ToString();
        }

        public double GetLatitude()
        {
            return(double.Parse(txtLatitude.Text));
        }


        public double GetLongitude()
        {
            return (double.Parse(txtLongitude.Text));
        }

        public string GetLatitudeStr()
        {
            return (txtLatitude.Text);
        }


        public string GetLongitudeStr()
        {
            return (txtLongitude.Text);
        }
    }
}
